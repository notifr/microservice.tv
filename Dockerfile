FROM openjdk:8-jdk
RUN mkdir /app
ARG ENV
ARG BACKEND-HOST

ADD config.properties /app/
ADD target/microservice.tv-0.0.1-SNAPSHOT-jar-with-dependencies.jar /app/

ENV ENV=$ENV
ENV BACKEND-HOST=$BACKEND-HOST

WORKDIR /app
CMD ["java","-jar","microservice.tv-0.0.1-SNAPSHOT-jar-with-dependencies.jar"]
