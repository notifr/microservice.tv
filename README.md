# JSON microservice:TV


## Subscrition/Subscriptions

```
{
    message: <string>
    data: {
        subscriptions: [
            {
                id: <integer>
                data: [
                    twenty: <boolean>
                    twentytwo: <boolean>
                    tipps: <boolean>
                    highlights: <boolean>
                ]
            }
        ]
    }
}
```
```
{
    message: <string>
    data: {
        subscribe: {
			id: <integer>
			data: [
				twenty: <boolean>
				twentytwo: <boolean>
				tipps: <boolean>
				highlights: <boolean>
			]
		}
        
    }
}
```



## unsubscribe

```

{
    message: <string>
    data: {
        subscriptions: [
            { 
                id: <integer>
            }
        ]
    }
}
```

## notify

```
{
    message: <string>
    data: {
        notificationMessage: <string>
        subscriptions: [
            { 
                id: <integer>
            }
        ]
    }
}
```

