package notifr.microservice.tv;

import static org.junit.Assert.*;

import java.util.ArrayList;

import notifr.microservice.tv.util.JsonHelper;

import org.junit.Before;
import org.junit.Test;

import com.google.gson.JsonObject;

public class JsonHelperTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testStartup() {
		JsonObject obj = JsonHelper.startup("test");
		assertEquals("{\"message\":\"test\",\"data\":{\"connector\":\"test\"}}", obj.toString());
	}

	@Test
	public void testNotifyStringArrayListOfString() {
		ArrayList<String> subs = new ArrayList<String>();
		subs.add("first");
		JsonObject obj = JsonHelper.notify("test", subs);
		assertEquals("{\"message\":\"notify\",\"data\":{\"notificationMessage\":\"test\",\"subscriptions\":[{\"id\":\"first\"}]}}", obj.toString());

		subs.add("second");
		subs.add("third");
		obj = JsonHelper.notify("test", subs);
		assertEquals(
				"{\"message\":\"notify\",\"data\":{\"notificationMessage\":\"test\",\"subscriptions\":[{\"id\":\"first\"},{\"id\":\"second\"},{\"id\":\"third\"}]}}",
				obj.toString());

		obj = JsonHelper.notify("test", new ArrayList<String>());
		assertEquals("{\"message\":\"notify\",\"data\":{\"notificationMessage\":\"test\",\"subscriptions\":[]}}", obj.toString());
	}

}
