package notifr.microservice.tv;

import static org.junit.Assert.*;

import java.net.MalformedURLException;
import java.net.URL;

import javax.xml.stream.XMLStreamException;

import notifr.microservice.tv.rss.model.FeedTvSpielfilm;
import notifr.microservice.tv.rss.read.ParserTVSpielfilm;

import org.junit.Before;
import org.junit.Test;

public class RssTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testParserTVSpielfilm() {
		ParserTVSpielfilm parser = null;
		FeedTvSpielfilm feed = null;
		try {
			URL feedUrl = new URL("http://www.tvspielfilm.de/tv-programm/rss/tipps.xml");
			parser = new ParserTVSpielfilm(feedUrl);
			feed = parser.readFeed();
		} catch (MalformedURLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (XMLStreamException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		assertEquals("", feed.getTitle());
		
		assertNotNull(feed.getMessages());
	}
}
