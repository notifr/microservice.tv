package notifr.microservice.tv;

import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import notifr.microservice.tv.persistence.AerospikeConnection;
import notifr.microservice.tv.rss.Element;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.Key;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;

public class AerospikeTest {
	private AerospikeConnection con = null;
	private AerospikeClient client = null;
	private String namespace = "test";

	@Before
	public void setUp() throws Exception {
		String aerospikeHost = "localhost";
		int aerospikePort = 3010;
		
		con = new AerospikeConnection(aerospikeHost, aerospikePort, "test");
		client = con.getClient();
	}
	
	@After
	public void tearDown() throws Exception {			
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		RecordSet records = client.query(null, stat);
		while (records.next()) {
			Key key = records.getKey();
			client.delete(null, key);
		}
	}

	@Test
	public void testInit() {
		ArrayList<String> indizes = new ArrayList<String>();
		ArrayList<String> indizesReal = new ArrayList<String>();

		assertEquals(indizes, indizesReal);
		
		indizes.add("twenty");
		indizes.add("twentytwo");
		indizes.add("highlights");
		indizes.add("tipps");
		//ToDo: get all indexes
	}

	@Test
	public void testExistRecordStringString() {
		String id = "test";
		String set = "test";
		assertEquals(con.existRecord(id, set), false);

		con.addRecord("test", "test", "test", "test");

		assertEquals(con.existRecord(id, set), true);

	}

	@Test
	public void testAddRecordElementString() {
		Element element = new Element("test", "test", "test");
		con.addRecord(element, "test");
		Key key = new Key(namespace, "test", "test");

		assertEquals(client.exists(null, key), true);
	}

	@Test
	public void testAddRecordStringStringStringString() {
		Key key = new Key(namespace, "test", "test");
		assertEquals(client.exists(null, key), false);

		con.addRecord("test", "test", "test", "test");

		assertEquals(client.exists(null, key), true);

	}
	@Test
	public void testGetAllRecords() {
		List<String> rec = new ArrayList<String>();
		String set = "test";
		assertEquals(rec, con.getAllRecords(set));
		
		rec.add("test");
		con.addRecord("test", "test", "test", set);
		assertEquals(rec, con.getAllRecords(set));		

		rec.add("test2");
		con.addRecord("test2", "test2", "test2", set);
		assertEquals(rec, con.getAllRecords(set));		

		con.addRecord("test3", "test3", "test3", "test2");
		assertEquals(rec, con.getAllRecords(set));
	}

	@Test
	public void testAddSubscription() {
		String id = "test";
		String subSet = "subs";
		Key key = new Key(namespace, subSet, id);
		assertEquals(client.exists(null, key), false);
		con.addSubscription(id, true, true, true, true);

		assertEquals(client.exists(null, key), true);
	}

	@Test
	public void testExistSubscription() {
		String id = "test";
		assertEquals(con.existSubscription(id), false);
		con.addSubscription(id, true, true, true, true);
		assertEquals(con.existSubscription(id), true);
	}

	@Test
	public void testGetSubscribers() {
		List<String> subs = new ArrayList<String>();
		assertEquals(subs, con.getSubscribers("twenty"));
		
		subs.add("test");
		con.addSubscription("test", true, true, true, true);
		assertEquals(subs, con.getSubscribers("twenty"));
		
		subs.add("test2");
		con.addSubscription("test2", true, true, true, true);
		assertEquals(subs, con.getSubscribers("twenty"));
		
		con.addSubscription("test3", false, true, true, true);
		assertEquals(subs, con.getSubscribers("twenty"));
	}

	@Test
	public void testClearSubscribers() {
		ArrayList<String> subs = new ArrayList<String>();
		con.addSubscription("test", true, true, true, true);
		con.clearSubscribers();
		assertEquals(subs, con.getSubscribers("twenty"));
		

		con.addSubscription("test1", true, true, true, true);
		con.addSubscription("test2", true, true, true, true);
		con.addSubscription("test3", true, true, true, true);
		con.clearSubscribers();
		assertEquals(subs, con.getSubscribers("twenty"));
	}

	@Test
	public void testClearRecords() {
		ArrayList<String> rec = new ArrayList<String>();
		String set = "test";
		assertEquals(rec, con.getAllRecords(set));
		
		con.addRecord("test", "test", "test", set);
		con.clearRecords(set);
		assertEquals(rec, con.getAllRecords(set));		

		con.addRecord("test", "test", "test", set);
		con.addRecord("test2", "test2", "test2", set);
		con.addRecord("test3", "test3", "test3", set);
		con.clearRecords(set);
		assertEquals(rec, con.getAllRecords(set));
		
		con.addRecord("test", "test", "test", set);
		con.addRecord("test2", "test2", "test2", set);
		con.addRecord("test3", "test3", "test3", "test3");
		con.clearRecords(set);
		assertEquals(rec, con.getAllRecords(set));
		rec.add("test3");
		assertEquals(rec, con.getAllRecords("test3"));
	}

}
