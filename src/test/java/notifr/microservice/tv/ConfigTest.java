package notifr.microservice.tv;

import static org.junit.Assert.*;

import java.util.HashMap;

import notifr.microservice.tv.rss.Config;

import org.junit.Before;
import org.junit.Test;

public class ConfigTest {

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testLoadFile() {
		//assertEquals("localhost", Config.getAerospikeHost());
		//assertEquals(3000, Config.getAerospikePort());

		Config.loadFile("config-test.properties", "twenty", "twentytwo",
				"tipps", "highlights");
		HashMap<String, String> channels = new HashMap<>();
		channels.put("twenty", "twenty");
		channels.put("twentytwo", "twentytwo");
		channels.put("tipps", "tipps");
		channels.put("highlights", "highlights");
		for(String value : channels.values() ){
			assertTrue(Config.getChannels().containsValue(value));
		}
//		Config.getSubKey()
//		Config.getPubKey() 
//		Config.getConnector()
//		Config.getChannel()
//		Config.getClientId()
//		Config.getUrl()
//		Config.getStartUrl()
//		Config.getProd()
//		Config.getTest()
	}
}
