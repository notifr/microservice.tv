package notifr.microservice.tv;

import static org.mockito.Mockito.*;
import static org.junit.Assert.*;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import notifr.microservice.tv.backend.BackendConnection;
import notifr.microservice.tv.persistence.AerospikeConnection;
import notifr.microservice.tv.rss.Element;
import notifr.microservice.tv.util.HttpHelper;
import notifr.microservice.tv.util.JsonHelper;

import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.runners.MockitoJUnitRunner;

import com.google.gson.JsonObject;

@RunWith(MockitoJUnitRunner.class)
public class BackendTest {
	private String clientId = "test";

	@Before
	public void setUp() throws Exception {
	}

	@Test
	public void testSendMessage() throws ClientProtocolException, IOException {
		HttpHelper http = new HttpHelper("", "");
		HttpHelper spyHttp = spy(http);

		BackendConnection backend = new BackendConnection(clientId, spyHttp);

		String message = "test";
		ArrayList<String> subs = new ArrayList<>();
		boolean res = backend.sendMessage(subs, message);
		doNothing().when(spyHttp).post(JsonHelper.notify(message, subs),
				clientId);
		assertTrue(res);

		subs.add("1");
		doNothing().when(spyHttp).post(JsonHelper.notify(message, subs),
				clientId);
		res = backend.sendMessage(subs, message);
		assertTrue(res);
		verify(spyHttp, times(1)).post(JsonHelper.notify(message, subs),
				clientId);

		subs.add("2");
		subs.add("3");
		subs.add("4");
		subs.add("5");
		doNothing().when(spyHttp).post(JsonHelper.notify(message, subs),
				clientId);
		res = backend.sendMessage(subs, message);
		assertTrue(res);
		verify(spyHttp, times(1)).post(JsonHelper.notify(message, subs),
				clientId);
	}

	@Test
	public void testStartMessage() throws ParseException, IOException {
		HttpHelper mockHttp = mock(HttpHelper.class);

		BackendConnection backend = new BackendConnection(clientId, mockHttp);
		JsonObject obj = new JsonObject();
		obj.addProperty("test", "test");

		when(mockHttp.getAtStart(clientId)).thenReturn(obj);

		assertEquals(obj, backend.startMessage());
	}

	@Test
	public void testSendNotification() throws ClientProtocolException, IOException {
		AerospikeConnection mockAero = mock(AerospikeConnection.class);
		HttpHelper mockHttp = mock(HttpHelper.class);
		BackendConnection backend = new BackendConnection(clientId, mockHttp);
		List<Element> ele = new ArrayList<Element>();
		String topic = "test";
		ArrayList<String> subs = new ArrayList<>();
		
		when(mockAero.getAllElements(topic)).thenReturn(ele);
		doNothing().when(mockHttp).post(new JsonObject(), clientId);
		assertTrue(backend.sendNotification(topic, subs, mockAero));
		
		subs.add("1");		
		ele.add(new Element("test1", "test1", "test1"));
		when(mockAero.getAllElements(topic)).thenReturn(ele);
		assertTrue(backend.sendNotification(topic, subs, mockAero));
		
		subs.add("1");
		subs.add("2");
		subs.add("3");		
		when(mockAero.getAllElements(topic)).thenReturn(ele);
		assertTrue(backend.sendNotification(topic, subs, mockAero));
	}

	@Test
	public void testGetHttphelper() {
		HttpHelper mock = new HttpHelper("", "");
		BackendConnection backend = new BackendConnection(clientId, mock);
		assertEquals(mock, backend.getHttphelper());
	}

	@Test
	public void testSetHttphelper() {
		HttpHelper mock = new HttpHelper("", "");
		BackendConnection backend = new BackendConnection(clientId, mock);
		backend.setHttphelper(mock);
		assertEquals(mock, backend.getHttphelper());
	}

	@Test
	public void testDestroy() {
		assertTrue(true);
	}

}
