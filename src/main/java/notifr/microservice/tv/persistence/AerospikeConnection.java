package notifr.microservice.tv.persistence;

import java.util.ArrayList;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.rss.Element;

import com.aerospike.client.AerospikeClient;
import com.aerospike.client.AerospikeException;
import com.aerospike.client.Bin;
import com.aerospike.client.Key;
import com.aerospike.client.Record;
import com.aerospike.client.query.Filter;
import com.aerospike.client.query.IndexType;
import com.aerospike.client.query.RecordSet;
import com.aerospike.client.query.Statement;

public class AerospikeConnection {
	private static final Logger LOG = LoggerFactory.getLogger(AerospikeConnection.class);
	private String address;
	private int port;
	private AerospikeClient client;
	private String namespace;
	private String subSet = "subs";
	private static final String TWENTY = "twenty";
	private static final String TWENTYTWO = "twentytwo";
	private static final String TIPPS = "tipps";
	private static final String HIGHLIGHTS = "highlights";

	public AerospikeConnection(String address, int port) {
		init(address, port, "TV");
	}

	public AerospikeConnection(String address, int port, String namespace) {
		init(address, port, namespace);
	}

	public void init(String address, int port, String namespace) {
		this.port = port;
		this.address = address;
		this.client = new AerospikeClient(this.address, this.port);
		this.namespace = namespace;
		client.createIndex(null, namespace, subSet, TWENTY, TWENTY, IndexType.STRING);
		client.createIndex(null, namespace, subSet, TWENTYTWO, TWENTYTWO, IndexType.STRING);
		client.createIndex(null, namespace, subSet, TIPPS, TIPPS, IndexType.STRING);
		client.createIndex(null, namespace, subSet, HIGHLIGHTS, HIGHLIGHTS, IndexType.STRING);
	}

	public List<String> getAllRecords(String set) {
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		stat.setSetName(set);
		return recordSetToArrayList(client.query(null, stat), "guid");
	}
	
	public List<Element> getAllElements(String set) {
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		stat.setSetName(set);
		return recordSetToArrayListElement(client.query(null, stat));
	}


	public boolean existRecord(Element e, String set) {
		return existRecord(e.getGuid(), set);
	}

	public boolean existRecord(String guid, String set) {
		Key key = new Key(namespace, set, guid);
		return client.exists(null, key);
	}

	public boolean addRecord(Element e, String set) {
		return addRecord(e.getPubdate(), e.getGuid(), e.getTitle(), set);
	}

	public boolean addRecord(String pubdate, String guid, String title,
			String set) {
		Key key = new Key(this.namespace, set, guid);
		Bin bin1 = new Bin("pubdate", pubdate);
		Bin bin2 = new Bin("title", title);
		Bin bin3 = new Bin("guid", guid);

		try {
			client.put(null, key, bin1, bin2, bin3);
			return true;
		} catch (AerospikeException ex) {
			LOG.error("record not added", ex);
			return false;
		}
	}

	public boolean addSubscription(String id, boolean twenty,
			boolean twentytwo, boolean tipps, boolean highlights) {
		Key key = new Key(this.namespace, this.subSet, id);
		Bin bin1 = new Bin("id", id);
		String str = Boolean.toString(twenty);
		Bin bin2 = new Bin(AerospikeConnection.TWENTY, str);
		str = Boolean.toString(twentytwo);
		Bin bin3 = new Bin(AerospikeConnection.TWENTYTWO, str);
		str = Boolean.toString(tipps);
		Bin bin4 = new Bin(AerospikeConnection.TIPPS, str);
		str = Boolean.toString(highlights);
		Bin bin5 = new Bin(AerospikeConnection.HIGHLIGHTS, str);
		try {
			client.put(null, key, bin1, bin2, bin3, bin4, bin5);
			return true;
		} catch (AerospikeException ex) {
			LOG.error("sbuscription not added", ex);
			return false;
		}
	}

	public boolean existSubscription(String id) {
		Key key = new Key(namespace, subSet, id);
		return client.exists(null, key);
	}

	public static List<Element> recordSetToArrayListElement(RecordSet set) {
		ArrayList<Element> list = new ArrayList<>();
		while (set.next()) {
			Record rec = set.getRecord();
			Element element = new Element(rec.getString("pubdate"),
					rec.getString("guid"), rec.getString("title"));
			list.add(element);
		}
		return list;
	}

	public List<String> recordSetToArrayList(RecordSet set,
			String element) {
		ArrayList<String> list = new ArrayList<>();
		while (set.next()) {
			Record rec = set.getRecord();
			list.add(rec.getString(element));
		}
		return list;
	}

	public List<String> getSubscribers(String bin) {
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		stat.setSetName(this.subSet);
		stat.setBinNames("id",bin);
		stat.setFilter(Filter.equal(bin, "true"));
		return recordSetToArrayList(client.query(null, stat), "id");
	}

	public void clearSubscribers() {
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		stat.setSetName(this.subSet);
		RecordSet set = client.query(null, stat);
		while (set.next()) {
			Key key = set.getKey();
			client.delete(null, key);
		}
	}

	public void clearRecords(String set) {
		Statement stat = new Statement();
		stat.setNamespace(this.namespace);
		stat.setSetName(set);
		RecordSet records = client.query(null, stat);
		while (records.next()) {
			Key key = records.getKey();
			client.delete(null, key);
		}
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public int getPort() {
		return port;
	}

	public void setPort(int port) {
		this.port = port;
	}

	public AerospikeClient getClient() {
		return client;
	}

	public void setClient(AerospikeClient client) {
		this.client = client;
	}

	public String getNamespace() {
		return namespace;
	}

	public void setNamespace(String namespace) {
		this.namespace = namespace;
	}
	public void destroy(){
		client.close();
	}
}
