package notifr.microservice.tv.rss;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Config {
	private static final Logger LOG = LoggerFactory.getLogger(Config.class);
	private static String prod = "";
	private static String test = "";
	private static String subKey = "";
	private static String pubKey = "";
	private static String connector = "";
	private static String channel = "";
	private static String clientId = "";
	private static String url = "";
	private static String startUrl = "";
	private static String aerospikeHost = "";
	private static int aerospikePort = 0;
	private static Map<String, String> channels = null;
	
	private Config(){}
	
	public static void loadFile( String name, String firstChannel,
			String secondChannel, String thirdChannel, String fourthChannel) {
		Properties prop = new Properties();

		try(InputStream input = new FileInputStream(name)) {
			// load a properties file from class path, inside static method
			prop.load(input);

			// get the property value and print it out

			prod = prop.getProperty("prod");
			test = prop.getProperty("test");
			subKey = prop.getProperty("subKey");
			pubKey = prop.getProperty("pubKey");
			connector = prop.getProperty("connector");
			channel = prop.getProperty("channel");
			clientId = prop.getProperty("clientId");
			
			String backend = System.getenv("BACKEND-HOST");
			url = prop.getProperty("urlPrefix") + backend + prop.getProperty("url");
			startUrl = prop.getProperty("urlPrefix") + backend +  prop.getProperty("startUrl");
			
			channels = new HashMap<>();
			channels.put(firstChannel, prop.getProperty(firstChannel));
			channels.put(secondChannel, prop.getProperty(secondChannel));
			channels.put(thirdChannel, prop.getProperty(thirdChannel));
			channels.put(fourthChannel, prop.getProperty(fourthChannel));

			String env = System.getenv("ENV");
			aerospikePort = 0;
			aerospikeHost = "aerospike-tv-" + env;
			if (env != null && env.equals(Config.getProd())) {
				aerospikePort = Integer.parseInt(prop
						.getProperty("aerospikePortProd"));
			} else if (env != null && env.equals(Config.getTest())) {
				aerospikePort = Integer.parseInt(prop
						.getProperty("aerospikePortTest"));
			} else {
				aerospikeHost = "localhost";
				aerospikePort = 3010;
			}

		} catch (IOException ex) {
			LOG.error("file not read",ex);
		} 
	}

	public static String getAerospikeHost() {
		return aerospikeHost;
	}

	public static void setAerospikeHost(String aerospikeHost) {
		Config.aerospikeHost = aerospikeHost;
	}

	public static int getAerospikePort() {
		return aerospikePort;
	}

	public static void setAerospikePort(int aerospikePort) {
		Config.aerospikePort = aerospikePort;
	}

	public static Map<String, String> getChannels() {
		return channels;
	}

	public static void setChannels(Map<String, String> channels) {
		Config.channels = channels;
	}

	public static String getSubKey() {
		return subKey;
	}

	public static void setSubKey(String subKey) {
		Config.subKey = subKey;
	}

	public static String getPubKey() {
		return pubKey;
	}

	public static void setPubKey(String pubKey) {
		Config.pubKey = pubKey;
	}

	public static String getConnector() {
		return connector;
	}

	public static void setConnector(String connector) {
		Config.connector = connector;
	}

	public static String getChannel() {
		return channel;
	}

	public static void setChannel(String channel) {
		Config.channel = channel;
	}

	public static String getClientId() {
		return clientId;
	}

	public static void setClientId(String clientId) {
		Config.clientId = clientId;
	}

	public static String getUrl() {
		return url;
	}

	public static void setUrl(String url) {
		Config.url = url;
	}

	public static String getStartUrl() {
		return startUrl;
	}

	public static void setStartUrl(String startUrl) {
		Config.startUrl = startUrl;
	}

	public static String getProd() {
		return prod;
	}

	public static void setProd(String prod) {
		Config.prod = prod;
	}

	public static String getTest() {
		return test;
	}

	public static void setTest(String test) {
		Config.test = test;
	}

}