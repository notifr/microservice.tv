package notifr.microservice.tv.rss;

import java.io.File;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Iterator;

import javax.xml.stream.XMLStreamException;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;

import notifr.microservice.tv.backend.BackendConnection;
import notifr.microservice.tv.persistence.AerospikeConnection;
import notifr.microservice.tv.rss.model.FeedMsgTvSpielfilm;
import notifr.microservice.tv.rss.model.FeedTvSpielfilm;
import notifr.microservice.tv.rss.read.ParserTVSpielfilm;
import notifr.microservice.tv.util.JsonHelper;
import notifr.microservice.tv.util.PubnubHelper;

public class Microservice {
	private static final Logger LOG = LoggerFactory
			.getLogger(Microservice.class);
	private BackendConnection backendCon;
	private AerospikeConnection aerospikeCon;
	private static final String FIRST = "twenty";
	private static final String SECOND = "twentytwo";
	private static final String THIRD = "highlights";
	private static final String FOURTH = "tipps";
	private static final String FIFTH = "local";
	private PubnubHelper pubnubhelper;
	private String local;

	public Microservice(String file, String local) {
		Config.loadFile(file, FIRST, SECOND, THIRD, FOURTH);
		this.local = local;
		String connector = Config.getConnector();

		this.aerospikeCon = new AerospikeConnection(Config.getAerospikeHost(),
				Config.getAerospikePort(), connector);
		this.backendCon = new BackendConnection(Config.getClientId(),
				Config.getUrl(), Config.getStartUrl());
		this.pubnubhelper = new PubnubHelper(Config.getSubKey(),
				Config.getPubKey(), Config.getChannel(), connector, backendCon,
				aerospikeCon);
		handleStartResponse(backendCon.startMessage());
	}

	public void clear() {
		clearRecords();
		aerospikeCon.clearSubscribers();
	}

	public void clearRecords() {
		aerospikeCon.clearRecords(FIRST);
		aerospikeCon.clearRecords(SECOND);
		aerospikeCon.clearRecords(THIRD);
		aerospikeCon.clearRecords(FOURTH);
		aerospikeCon.clearRecords(FIFTH);
	}

	public void checkAll() {
		String check = "Checking ";
		LOG.info(check + FIRST);
		loadRss(Config.getChannels().get(FIRST), FIRST);
		LOG.info(check + SECOND);
		loadRss(Config.getChannels().get(SECOND), SECOND);
		LOG.info(check + THIRD);
		loadRss(Config.getChannels().get(THIRD), THIRD);
		LOG.info(check + FOURTH);
		loadRss(Config.getChannels().get(FOURTH), FOURTH);
		LOG.info(check + FIFTH);
	}

	public void loadRss(String url, String name) {
		ParserTVSpielfilm parser = null;
		try {
			URL feedUrl = new URL(url);
			parser = new ParserTVSpielfilm(feedUrl);
			utiliseFeed(parser.readFeed(), name);
		} catch (MalformedURLException e) {
			LOG.warn("URL-Problem", e);
		} catch (XMLStreamException e) {
			LOG.warn("XML-Problem", e);
		}
	}

	public void loadLocal() {
		ParserTVSpielfilm parser = null;
		try {
			URL feedUrl = new File(local).toURI().toURL();
			parser = new ParserTVSpielfilm(feedUrl);
			utiliseFeed(parser.readFeed(), FIFTH);
		} catch (MalformedURLException e) {
			LOG.warn("URL-Problem", e);
		} catch (XMLStreamException e) {
			LOG.warn("XML-Problem", e);
		}
	}

	public void utiliseFeed(FeedTvSpielfilm feed, String bin) {
		for (FeedMsgTvSpielfilm message : feed.getMessages()) {
			LOG.info(message.getTitle());
			Element e = Element.elementFromXml(message);
			if (!aerospikeCon.existRecord(e, bin)) {
				aerospikeCon.addRecord(e, bin);
				ArrayList<String> list = (ArrayList<String>) aerospikeCon
						.getSubscribers(bin);
				backendCon.sendMessage(list, message.getTitle());
			}

		}
	}

	public boolean handleStartResponse(JsonObject response) {

		JsonArray array = response.get("data").getAsJsonObject()
				.get("subscriptions").getAsJsonArray();
		Iterator<JsonElement> arrayIter = array.iterator();
		while (arrayIter.hasNext()) {
			JsonObject obj = arrayIter.next().getAsJsonObject();
			String id = obj.get("id").getAsString();
			JsonObject data = (JsonObject) obj.get("data");
			if (!aerospikeCon.existSubscription(id)) {
				boolean firstB = JsonHelper.getJsonBoolean(data, FIRST);
				boolean secondB = JsonHelper.getJsonBoolean(data, SECOND);
				boolean thirdB = JsonHelper.getJsonBoolean(data, THIRD);
				boolean fourthB = JsonHelper.getJsonBoolean(data, FOURTH);

				aerospikeCon.addSubscription(id, firstB, secondB, thirdB,
						fourthB);
				ArrayList<String> subs = new ArrayList<>();
				subs.add(id);

				if (firstB) {
					backendCon.sendNotification(FIRST, subs, aerospikeCon);
				}
				if (secondB) {
					backendCon.sendNotification(SECOND, subs, aerospikeCon);
				}
				if (thirdB) {
					backendCon.sendNotification(THIRD, subs, aerospikeCon);
				}
				if (fourthB) {
					backendCon.sendNotification(FOURTH, subs, aerospikeCon);
				}
				backendCon.sendNotification(FIFTH, subs, aerospikeCon);
				aerospikeCon.clearRecords(FIFTH);
			}
		}
		return true;
	}

	public void destroy() {
		backendCon.destroy();
		aerospikeCon.destroy();
		pubnubhelper.destroy();
	}
}