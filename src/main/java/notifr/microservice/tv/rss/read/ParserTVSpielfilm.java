package notifr.microservice.tv.rss.read;

import java.io.IOException;
import java.io.InputStream;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.HashMap;
import java.util.Map;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.rss.model.FeedMsgTvSpielfilm;
import notifr.microservice.tv.rss.model.FeedTvSpielfilm;
import notifr.microservice.tv.util.RssHelper;

public class ParserTVSpielfilm {
	private static final Logger LOG = LoggerFactory.getLogger(ParserTVSpielfilm.class);
	static final String TITLE = "title";
	static final String DESCRIPTION = "description";
	static final String LINK = "link";
	static final String CONTENT = "content:encoded";
	static final String ITEM = "item";
	static final String PUBDATE = "pubDate";
	static final String GUID = "guid";
	final URL url;

	public ParserTVSpielfilm(URL feedUrl) throws MalformedURLException {
		this.url = feedUrl;
	}

	public FeedTvSpielfilm readFeed() throws XMLStreamException {
		FeedTvSpielfilm feed = null;
		try {
			// First create a new XMLInputFactory
			XMLInputFactory inputFactory = XMLInputFactory.newInstance();
			// Setup a new eventReader
			InputStream in = RssHelper.read(url);
			XMLEventReader eventReader = inputFactory.createXMLEventReader(in);
			// read the XML document

			feed = workEventReader(eventReader);
		} catch (IOException e) {
			LOG.warn("XML not read", e);
		}
		return feed;
	}

	private FeedTvSpielfilm workEventReader(XMLEventReader eventReader) {
		FeedTvSpielfilm feed = new FeedTvSpielfilm("", "", "", "", "", "", "");
		Map<String, String> map = new HashMap<>();
		try {
			while (eventReader.hasNext()) {
				XMLEvent event = eventReader.nextEvent();
				if (event.isStartElement()) {
					String localPart = event.asStartElement().getName()
							.getLocalPart();
					switch (localPart) {
					case PUBDATE:
					case GUID:
					case TITLE:
					case LINK:
					case DESCRIPTION:
					case CONTENT:
						map.put(localPart,
								RssHelper.getCharacterData(eventReader));
						break;
					default:
						break;
					}
				} else if (event.isEndElement()) {
					if (event.asEndElement().getName().getLocalPart() == (ITEM)) {
						FeedMsgTvSpielfilm message = new FeedMsgTvSpielfilm(map);
						feed.getMessages().add(message);
						continue;
					}
				}
			}
		} catch (XMLStreamException e) {
			LOG.warn("Parser Problem", e);
		}
		return feed;
	}
}
