package notifr.microservice.tv.rss;

import notifr.microservice.tv.rss.model.FeedMsgTvSpielfilm;

public class Element {

	private String pubdate;
	private String guid;
	private String title;
	
	public Element(String pubdate, String guid, String title) {
		this.pubdate = pubdate;
		this.guid = guid;
		this.title = title;
	}
	
	public static Element elementFromXml(FeedMsgTvSpielfilm xml){
		return new Element(xml.getPubdate(), xml.getGuid(), xml.getTitle());
	}
	
	public String getPubdate() {
		return pubdate;
	}
	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}
	public String getGuid() {
		return guid;
	}
	public void setGuid(String guid) {
		this.guid = guid;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
}
