package notifr.microservice.tv.rss.model;

import java.util.ArrayList;
import java.util.List;

public class FeedTvSpielfilm {

	        final String title;
	        final String link;
	        final String description;
	        final String language;
	        final String category;
	        final String generator;
	        final String image;

	        final List<FeedMsgTvSpielfilm> entries = new ArrayList<>();

	        public FeedTvSpielfilm(String title, String link, String description, String language,
	                        String category, String generator, String image) {
	                this.title = title;
	                this.link = link;
	                this.description = description;
	                this.language = language;
	                this.category = category;
	                this.generator = generator;
	                this.image = image;
	        }

	        public List<FeedMsgTvSpielfilm> getMessages() {
	                return entries;
	        }

	        public String getTitle() {
	                return title;
	        }

	        public String getLink() {
	                return link;
	        }

	        public String getDescription() {
	                return description;
	        }

	        public String getLanguage() {
	                return language;
	        }

	        public String getCategory() {
	                return category;
	        }

	        public String getGenerator() {
	                return generator;
	        }

	        @Override
	        public String toString() {
	                return "";
	        }

}
