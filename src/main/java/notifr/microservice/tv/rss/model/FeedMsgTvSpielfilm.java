package notifr.microservice.tv.rss.model;

import java.util.Map;

public class FeedMsgTvSpielfilm {
	private String pubdate;
	private String guid;
	private String title;
	private String link;
	private String description;
	private String content;
	
	public FeedMsgTvSpielfilm(Map<String, String> map){
		this.pubdate = map.get("pubdate");
		this.guid = map.get("guid");
		this.title = map.get("title");
		this.link = map.get("link");
		this.description = map.get("description");
		this.content = map.get("content:encoded");
	}
	
	public String getPubdate() {
		return pubdate;
	}

	public void setPubdate(String pubdate) {
		this.pubdate = pubdate;
	}

	public String getGuid() {
		return guid;
	}

	public void setGuid(String guid) {
		this.guid = guid;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getLink() {
		return link;
	}

	public void setLink(String link) {
		this.link = link;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getContent() {
		return content;
	}

	public void setContent(String content) {
		this.content = content;
	}
	@Override
	public String toString(){
		return content;
	}

}
