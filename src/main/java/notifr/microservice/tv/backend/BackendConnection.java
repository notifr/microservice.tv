package notifr.microservice.tv.backend;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.persistence.AerospikeConnection;
import notifr.microservice.tv.rss.Element;
import notifr.microservice.tv.util.HttpHelper;
import notifr.microservice.tv.util.JsonHelper;

import com.google.gson.JsonObject;

public class BackendConnection {
	private String clientId;
	private HttpHelper httphelper;
	private static final Logger LOG = LoggerFactory.getLogger(BackendConnection.class);

	public BackendConnection(String clientId, String url, String startUrl) {
		this.clientId = clientId;
		this.httphelper = new HttpHelper(url, startUrl);
	}

	public BackendConnection(String clientId, HttpHelper httphelper) {
		this.clientId = clientId;
		this.httphelper = httphelper;
	}

	public boolean sendMessage(List<String> subs, String message) {
		if (subs.isEmpty()) {
			return true;
		} else {
			return post(JsonHelper.notify(message, subs));
		}
	}

	public JsonObject startMessage() {
		return startupPost();
	}

	private boolean post(JsonObject message) {
		boolean ret = false;
		try {
			httphelper.post(message, clientId);
			ret = true;
		} catch (Exception ex) {
			LOG.warn("Post Problem", ex);
		}
		return ret;
	}

	private JsonObject startupPost() {
		try {
			LOG.info("Send startup");
			return httphelper.getAtStart(clientId);
		} catch (Exception ex) {
			LOG.warn("Post Problem", ex);
			return null;
		}
	}

	public boolean sendNotification(String topic, List<String> subs,
			AerospikeConnection aerospikeCon) {
		List<Element> list = aerospikeCon.getAllElements(topic);
		for (Element element : list) {
			if (sendMessage(subs, element.getTitle()) == false)
				return false;
		}
		return true;
	}

	public HttpHelper getHttphelper() {
		return httphelper;
	}

	public void setHttphelper(HttpHelper httphelper) {
		this.httphelper = httphelper;
	}

	public void destroy() {
		httphelper.destroy();
	}
}
