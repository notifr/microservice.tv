package notifr.microservice.tv.util;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.Characters;
import javax.xml.stream.events.XMLEvent;

import java.net.URL;

public class RssHelper {

	private RssHelper(){}

	public static String getCharacterData(XMLEventReader eventReader)
			throws XMLStreamException {
		String result = "";
		XMLEvent event = eventReader.nextEvent();
		if (event instanceof Characters) {
			result = event.asCharacters().getData();
		}
		return result;
	}

	public static InputStream read(URL url) throws IOException {
		try {
			return url.openStream();
		} catch (IOException e) {
			throw e;
		}
	}
	public static InputStream read(String file) throws IOException {
		try {
			return new FileInputStream(file);
		} catch (IOException e) {
			throw e;
		}
	}

}
