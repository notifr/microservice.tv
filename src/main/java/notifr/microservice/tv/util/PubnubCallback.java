package notifr.microservice.tv.util;

import java.util.ArrayList;
import java.util.Iterator;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.backend.BackendConnection;
import notifr.microservice.tv.persistence.AerospikeConnection;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.pubnub.api.PubNub;
import com.pubnub.api.callbacks.SubscribeCallback;
import com.pubnub.api.enums.PNStatusCategory;
import com.pubnub.api.models.consumer.PNStatus;
import com.pubnub.api.models.consumer.pubsub.PNMessageResult;
import com.pubnub.api.models.consumer.pubsub.PNPresenceEventResult;

public class PubnubCallback extends SubscribeCallback {
	private static final Logger LOG = LoggerFactory.getLogger(PubnubCallback.class);
	private String channel;
	private String connector;
	private BackendConnection backendCon;
	private AerospikeConnection aerospikeCon;
	private static final String FIRST = "twenty";
	private static final String SECOND = "twentytwo";
	private static final String THIRD = "highlights";
	private static final String FOURTH = "tipps";
	private static final String FIFTH = "local";

	public PubnubCallback(String channel, String connector,
			BackendConnection backendCon, AerospikeConnection aerospikeCon) {
		this.channel = channel;
		this.connector = connector;
		this.backendCon = backendCon;
		this.aerospikeCon = aerospikeCon;
	}

	@Override
	public void message(PubNub arg0, PNMessageResult message) {
		if (message.toString().equals(channel)) {
			JsonArray array = message.getMessage().getAsJsonObject()
					.get("data").getAsJsonObject().get("subscribe")
					.getAsJsonArray();
			Iterator<JsonElement> arrayIter = array.iterator();
			while (arrayIter.hasNext()) {
				JsonObject obj = arrayIter.next().getAsJsonObject();
				String id = obj.get("id").getAsString();
				JsonObject data = (JsonObject) obj.get("data");
				if (!aerospikeCon.existSubscription(id)) {
					boolean firstB = JsonHelper.getJsonBoolean(data, FIRST);
					boolean secondB = JsonHelper.getJsonBoolean(data, SECOND);
					boolean thirdB = JsonHelper.getJsonBoolean(data, THIRD);
					boolean fourthB = JsonHelper.getJsonBoolean(data, FOURTH);
								
					aerospikeCon.addSubscription(id, firstB, secondB, thirdB, fourthB);
					ArrayList<String> subs = new ArrayList<String>();
					subs.add(id);
					
					if (firstB) {
						backendCon.sendNotification(FIRST, subs, aerospikeCon);
					}
					if (secondB) {
						backendCon.sendNotification(SECOND, subs, aerospikeCon);
					}
					if (thirdB) {
						backendCon.sendNotification(THIRD, subs, aerospikeCon);
					}
					if (fourthB) {
						backendCon.sendNotification(FOURTH, subs, aerospikeCon);
					}
					backendCon.sendNotification(FIFTH, subs, aerospikeCon);
				}
			}
		}
	}

	@Override
	public void presence(PubNub arg0, PNPresenceEventResult arg1) {
		// not used
	}

	@Override
	public void status(PubNub arg0, PNStatus status) {
		if (status.getCategory() == PNStatusCategory.PNConnectedCategory) {
			LOG.info("Pubnub connected for " + connector
					+ " channel " + channel);
		}
	}

}
