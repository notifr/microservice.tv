package notifr.microservice.tv.util;

import java.util.Arrays;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.backend.BackendConnection;
import notifr.microservice.tv.persistence.AerospikeConnection;

import com.pubnub.api.PNConfiguration;
import com.pubnub.api.PubNub;

public class PubnubHelper {
	private static final Logger LOG = LoggerFactory.getLogger(PubnubHelper.class);
	private PubNub pubnub;

	public PubnubHelper(String subKey, String pubKey, String channel,
			String connector, BackendConnection backendCon, AerospikeConnection aerospikeCon) {
		PNConfiguration pnConfiguration = getConfig(subKey, pubKey);
		this.pubnub = new PubNub(pnConfiguration);
		LOG.info(pubnub.getPresenceState().toString());

		addListener(channel, connector, backendCon, aerospikeCon);
	}

	private PNConfiguration getConfig(String subKey, String pubKey) {
		PNConfiguration pnConfiguration = new PNConfiguration();
		pnConfiguration.setSubscribeKey(subKey);
		pnConfiguration.setPublishKey(pubKey);
		pnConfiguration.setSecure(false);
		return pnConfiguration;

	}

	public void addListener(String channel, String connector,
			BackendConnection backendCon, AerospikeConnection aerospikeCon) {
		PubnubCallback callback = new PubnubCallback(channel, connector, backendCon, aerospikeCon);
		pubnub.addListener(callback);
		pubnub.subscribe().channels(Arrays.asList(channel)).execute();

	}

	public void destroy() {
		pubnub.destroy();
	}

	public PubNub getPubnub() {
		return pubnub;
	}

	public void setPubnub(PubNub pubnub) {
		this.pubnub = pubnub;
	}
	

}
