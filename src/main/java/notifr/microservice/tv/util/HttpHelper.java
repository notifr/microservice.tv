package notifr.microservice.tv.util;

import java.io.IOException;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.HttpClientBuilder;
import org.apache.http.util.EntityUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;

public class HttpHelper {
	private static final Logger LOG = LoggerFactory.getLogger(HttpHelper.class);
	private String url;
	private String startUrl;

	public HttpHelper(String url, String startUrl) {
		this.url = url;
		this.startUrl = startUrl;
	}

	public void post(JsonObject message, String clientId)
			throws ClientProtocolException, IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpPost post = new HttpPost(url);
		StringEntity postingString = new StringEntity(message.toString());
		post.setEntity(postingString);
		post.setHeader("Content-type", "application/json");
		post.setHeader("Cookie", "PHPSESSID=" + clientId);

		LOG.info("Send post");
		 httpClient.execute(post);
	}

	public JsonObject getAtStart(String clientId) throws ParseException,
			IOException {
		HttpClient httpClient = HttpClientBuilder.create().build();
		HttpGet get = new HttpGet(startUrl);
		get.setHeader("Content-type", "application/json");
		get.setHeader("Cookie", "PHPSESSID=" + clientId);
		HttpResponse response = httpClient.execute(get);

		JsonParser parser = new JsonParser();
		HttpEntity entity = response.getEntity();
		String responseString = EntityUtils.toString(entity, "UTF-8");
		return parser.parse(responseString).getAsJsonObject();
	}
	public void destroy(){
		// send destroymessage, not implemented
	}
}
