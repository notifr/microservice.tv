package notifr.microservice.tv.util;

import java.util.List;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;

public class JsonHelper {
	
	private JsonHelper(){}

	public static JsonObject startup(String connector) {

		JsonObject object = new JsonObject();
		object.addProperty("message", "test");
		JsonObject data = new JsonObject();
		data.addProperty("connector", connector);
		object.add("data", data);
		return object;
	}
	
	public static JsonObject notify(String message, List<String> subs){
		JsonObject object = new JsonObject();
		object.addProperty("message", "notify");
		JsonObject data = new JsonObject();
		data.addProperty("notificationMessage", message);
		JsonArray array = new JsonArray();
		for(String sub : subs){
			JsonObject subscriber = new JsonObject();
			subscriber.addProperty("id", sub);
			array.add(subscriber);	
		}
		data.add("subscriptions", array);
		object.add("data", data);
		return object;
	}

	public static boolean getJsonBoolean(JsonObject data, String name) {
		return data.get(name).getAsJsonPrimitive().getAsBoolean();

	}

}
