package notifr.microservice.tv;

import java.util.Timer;
import java.util.TimerTask;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import notifr.microservice.tv.rss.Microservice;

public class Main {
	private static final Logger LOG = LoggerFactory.getLogger(Microservice.class);
	private Main(){}

	public static void main(String[] args) {
		String configfile = "config.properties";
		String localrss = "local.xml";
		Microservice service = new Microservice(configfile, localrss);
		TimerTask action = new TimerTask() {
			@Override
			public void run() {
				LOG.info("Checking all...");
				service.checkAll();
				LOG.info("Done checking...");
			}

		};
		Timer caretaker = new Timer();
		caretaker.schedule(action, 1000, 60000);

	}

}
